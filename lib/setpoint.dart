import 'package:flutter/painting.dart';

enum SetpointSide { left, right, both }

/// Temperature setpoint.
///
/// This class represents a setpoint mark, which is displayed
/// as two triangles on both side of the termometer bar.
///
class Setpoint {
  static const kSetpointSize = 8.0;
  static const kSetpointColor = Color.fromRGBO(255, 0, 0, 1.0);

  /// The value (temperature) of the setpoint.
  final double value;

  /// The size of the setpoint mark.
  final double size;

  /// The color of the setpoint mark.
  final Color color;

  /// The side on which the setpoint mark is drawn.
  final SetpointSide side;

  Setpoint(double value,
      {double? size, Color? color, required SetpointSide side})
      : value = value,
        size = size ?? kSetpointSize,
        color = color ?? kSetpointColor,
        side = side;

  /// Creates a new copy of the object, altering the specified properties.
  Setpoint apply(
          {double? value, double? size, Color? color, SetpointSide? side}) =>
      Setpoint(value ?? this.value,
          size: size ?? this.size,
          color: color ?? this.color,
          side: side ?? this.side);
}
