import 'package:flutter/rendering.dart';

/// Descibes a single scale tick.
///
/// Holds all the properties requried to draw a single scale tick mark.
class ScaleTick {
  static const TextStyle kTickTextStyle = TextStyle(fontSize: 10);
  static const double kTickLength = 8.0;
  static const double kTickLabelSpace = 5.0;
  static const double kTickThickness = 1.0;

  /// The value (temperature) of the tick.
  final double value;

  /// The length of the tick (perpendicular to the outline), in pixels.
  final double length;

  /// The thickness of the tick, in pixels. If not defined, the thickness
  /// defaults to the outline thickness.
  // final double thickness;
  double thickness;

  /// The text label attached to the tick.
  final String label;

  /// The space, in pixels between the tick mark and the label.
  final double labelSpace;

  /// The text style used for drawing the tick label. If the color in the label
  /// is not defined, it defaults to the outline color.
  final TextStyle textStyle;

  /// Create a scale tick.
  ///
  /// [value] is mandatory and must not be null.
  ScaleTick(this.value,
      {required label,
      double? length,
      double? thickness,
      TextStyle? textStyle,
      double? labelSpace})
      : label = label,
        length = length ?? kTickLength,
        thickness = thickness ?? kTickThickness,
        textStyle = textStyle ?? kTickTextStyle,
        labelSpace = labelSpace ?? kTickLabelSpace;
}

/// Interface for a class providing scale ticks.
///
/// A class implementing this interface calculates a list of scale
/// ticks to draw on the thermometer. Each tick can be individually tuned for
/// various properties such as color, style and label.
abstract class ScaleProvider {
  /// Calculate the list of ticks to display.
  ///
  /// For a given range (minValue to maxValue), calculates a list
  /// of scale ticks to be displayed.
  List<ScaleTick> calcTicks(double minValue, double maxValue);
}

/// Provides scale ticks at constant intervals.
class IntervalScaleProvider extends ScaleProvider {
  /// The temperature interval on which to draw the tick marks.
  final double interval;

  /// Number of fraction digits to show on the each number.
  final int fractionDigits;

  /// The length of the all the tick mark.
  final double length;

  /// The thickness of all the tick marks.
  // final double? thickness;
  double thickness;

  /// Creates an interval scale provider.
  ///
  /// The [interval] must be non-null and greater than zero. All other parameters are
  /// optional, defaults will be used if not specified.
  IntervalScaleProvider(double interval,
      {int? fractionDigits, double? length, double? thickness})
      : this.interval = interval,
        this.fractionDigits = fractionDigits ?? 1,
        this.length = length ?? ScaleTick.kTickLength,
        this.thickness = thickness ?? ScaleTick.kTickThickness {
    assert(this.interval > 0);
    assert(this.fractionDigits >= 0);
    assert(this.length > 0);
    assert(this.thickness > 0);
  }

  @override
  List<ScaleTick> calcTicks(double minValue, double maxValue) {
    double v = minValue;
    List<ScaleTick> ticks = [];

    while (v <= maxValue) {
      ticks.add(ScaleTick(v,
          label: v.toStringAsFixed(fractionDigits),
          length: length,
          thickness: thickness));
      v += interval;
    }

    return ticks;
  }

  /// Creates a new copy of the object, altering the specified properties.
  IntervalScaleProvider apply(double? interval, int? fractionDigits,
          double? length, double? thickness) =>
      IntervalScaleProvider(interval ?? this.interval,
          fractionDigits: fractionDigits ?? this.fractionDigits,
          length: length ?? this.length,
          thickness: thickness ?? this.thickness);
}
